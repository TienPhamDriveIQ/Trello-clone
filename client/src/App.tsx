import React from "react";
import Trello from "./container/Trello";

function App() {
  return <Trello />;
}

export default App;
